# Coding Dojo

[![pipeline status](https://gitlab.com/gsalaun1/coding-dojo-dm/badges/main/pipeline.svg)](https://gitlab.com/gsalaun1/coding-dojo-dm/-/commits/main)
 
# Slides

[Lien](https://gsalaun1.gitlab.io/coding-dojo-dm/)

# TDD

![TDD Cycle](slides/img/tdd.png "TDD Cycle")

## Three laws of TDD

1. You are not allowed to write any production code unless it is to make a failing unit test pass.
2. You are not allowed to write any more of a unit test than is sufficient to fail; and compilation failures are failures.
3. You are not allowed to write any more production code than is sufficient to pass the one failing unit test.

[The Three Laws of TDD (Featuring Kotlin)](https://www.youtube.com/watch?v=qkblc5WRn-U)

# Roman Numerals

| Symbol  | Value  |
|---------|--------|
| I       | 1      |
| V       | 5      |
| X       | 10     |
| L       | 50     |
| C       | 100    |
| D       | 500    |
| M       | 1000   |

## Part I

Write a function to convert from normal numbers to Roman Numerals

## Part II

Write a function to convert in the other direction, ie numeral to digit

## Rules
- Hardcore TDD. No Excuses!
- Change roles (driver, navigator) after each TDD cycle.
- No red phases while refactoring.
- There is no need to be able to convert numbers larger than about 3000.

